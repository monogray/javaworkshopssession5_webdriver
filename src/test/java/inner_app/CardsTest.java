package inner_app;

import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;
import page_object.*;
import utils.*;

import java.sql.Timestamp;
import java.util.*;

public class CardsTest {

    @Test
    public void cardCreateAndEdit_v2() throws Exception {

        new LandingPage(DriverHolder.get())
                .logOutIfLogIned()
                .open()
                .goToLoginPage()
                .login("e.gubich+test@helsi.me", "test123456789")
                .checkIsLogined("Test (test00176474)")
                .getHeader().logOut();
                //.openBoard("Доска без названия")

    }

    @Test
    public void cardCreateAndEdit_v3() throws Exception {

        new LandingPage(DriverHolder.get())
                .logOutIfLogIned()
                .open()
                .goToLoginPage()
                .login("e.gubich+test@helsi.me", "test123456789")
                .checkIsLogined("Test (test00176474)")
                .getHeader().logOut();
                //.openBoard("Доска без названия")
    }










    // @Test
    public void cardCreateAndEdit() throws Exception {
        System.setProperty("webdriver.chrome.driver", "/Users/user/Dropbox/Development/java-projects/Libraries/lib/selenium/chromedriver");
        WebDriver driver = new ChromeDriver();

        WebDriverWait wait = new WebDriverWait(driver, 10);


        driver.get("https://trello.com/");

        WebElement loginButton = driver.findElement( By.linkText("Log In") );
        loginButton.click();

        WebElement loginField = driver.findElement( By.id("user") );
        WebElement passwordField = driver.findElement( By.id("password") );

        loginField.sendKeys("e.gubich+test@helsi.me");
        passwordField.sendKeys("test123456789");
        passwordField.submit();




        wait.until(ExpectedConditions.visibilityOfElementLocated( By.xpath("//span[@title=\"Test (test00176474)\"]") ));
        WebElement userIcon = driver.findElement( By.xpath("//span[@title=\"Test (test00176474)\"]") );
        userIcon.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated( By.xpath("//span[@class=\"pop-over-header-title\"]") ));
        WebElement userNameElem = driver.findElement( By.xpath("//span[@class=\"pop-over-header-title\"]") );
        String userName = userNameElem.getText();

        Assert.assertEquals("Test (test00176474)", userName);





        wait.until(ExpectedConditions.visibilityOfElementLocated( By.xpath("//div[contains(text(), \"Доска\")]") ));
        WebElement testBoard = driver.findElement( By.xpath("//div[contains(text(), \"Доска\")]") );
        testBoard.click();







        String createCardButtonXPath = "//h2[contains(text(), \"Готово\")]/../..//span[contains(text(), \"Добавить\")][2]";
        wait.until(ExpectedConditions.visibilityOfElementLocated( By.xpath(createCardButtonXPath) ));

        WebElement createCardButton = driver.findElement( By.xpath(createCardButtonXPath) );
        createCardButton.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated( By.xpath("//textarea[@placeholder=\"Ввести заголовок для этой карточки\"]") ));
        WebElement cardNameField = driver.findElement( By.xpath("//textarea[@placeholder=\"Ввести заголовок для этой карточки\"]") );

        String newCardName = "Валик чемпион! " + new Timestamp(System.currentTimeMillis()).getTime();
        cardNameField.sendKeys(newCardName, Keys.ENTER);

        wait.until(ExpectedConditions.visibilityOfElementLocated( By.xpath("//h2[contains(text(), \"Готово\")]/../..//span[contains(text(), \"" + newCardName + "\")]") ));
        WebElement createdCard = driver.findElement( By.xpath("//h2[contains(text(), \"Готово\")]/../..//span[contains(text(), \"" + newCardName + "\")]") );

        Actions actions = new Actions(driver);
        actions.moveToElement(createdCard).build().perform();

        String editButtonXPath = "//h2[contains(text(), \"Готово\")]/../..//span[contains(text(), \"" + newCardName + "\")]/../..//span[contains(@class,\"icon-edit\")]";
        wait.until(ExpectedConditions.visibilityOfElementLocated( By.xpath(editButtonXPath) ));
        WebElement editButton = driver.findElement( By.xpath(editButtonXPath) );
        editButton.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated( By.xpath("//textarea[contains(text(), \"" + newCardName + "\")]") ));
        cardNameField = driver.findElement( By.xpath("//textarea[contains(text(), \"" + newCardName + "\")]") );

        cardNameField.sendKeys(Keys.ENTER);







        WebElement lastAddCard = driver.findElement(By.xpath("(//div[h2[contains(text(),\"Готово\")]]/..//span[contains(@class,\"list-card-title js-card-name\")])[last()]"));
        wait.until(ExpectedConditions.visibilityOf(lastAddCard));
        String lastAddCardText = lastAddCard.getText();

        WebElement inprogressList = driver.findElement(By.xpath("//h2[contains(text(),\"В процессе\")]/../.."));

        List<WebElement> searchArticle1 = driver.findElements(By.xpath("//h2[contains(text(),\"В процессе\")]/../..//a[contains(@class,\"list-card\")]"));
        int beforDragAndDrop = searchArticle1.size();

        new Actions(driver).dragAndDrop(lastAddCard, inprogressList).perform();

        List<WebElement> searchArticle2 = driver.findElements(By.xpath("//h2[contains(text(),\"В процессе\")]/../..//a[contains(@class,\"list-card\")]"));
        int afterDragAndDrop = searchArticle2.size();

        Iterator iterator = searchArticle2.iterator();
        while (iterator.hasNext() && afterDragAndDrop > beforDragAndDrop) {
            WebElement element = (WebElement) iterator.next();
            Assert.assertTrue("There now ticket name: \"" + lastAddCardText + "\" in search result", element.getText().toLowerCase().contains(lastAddCardText));
        }

        // TODO ...


        driver.close();
    }

    //@Test
    //@Before
    public void cardDelete() throws Exception {
        System.setProperty("webdriver.chrome.driver", "/Users/user/Dropbox/Development/java-projects/Libraries/lib/selenium/chromedriver");
        WebDriver driver = new ChromeDriver();

        WebDriverWait wait = new WebDriverWait(driver, 10);


        driver.get("http://htmlbook.ru/html/select");


        WebElement selectElem = driver.findElement(By.name("select2"));
        Select select = new Select(selectElem);
        select.selectByVisibleText("Крыса Лариса");





    }

    // @Test
    public void cardAssigned() throws Exception {
        System.setProperty("webdriver.chrome.driver", "/Users/user/Dropbox/Development/java-projects/Libraries/lib/selenium/chromedriver");
        WebDriver driver = new ChromeDriver();

        WebDriverWait wait = new WebDriverWait(driver, 10);


        driver.get("https://trello.com/");

        WebElement loginButton = driver.findElement( By.linkText("Log In") );
        loginButton.click();

        WebElement loginField = driver.findElement( By.id("user") );
        WebElement passwordField = driver.findElement( By.id("password") );

        loginField.sendKeys("e.gubich+test@helsi.me");
        passwordField.sendKeys("test123456789");
        passwordField.submit();

        wait.until(ExpectedConditions.visibilityOfElementLocated( By.xpath("//div[contains(text(), \"Доска\")]") ));
        WebElement testBoard = driver.findElement( By.xpath("//div[contains(text(), \"Доска\")]") );
        testBoard.click();


        String createCardButtonXPath = "//h2[contains(text(), \"Готово\")]/../..//span[contains(text(), \"Добавить\")][2]";
        wait.until(ExpectedConditions.visibilityOfElementLocated( By.xpath(createCardButtonXPath) ));

        WebElement createCardButton = driver.findElement( By.xpath(createCardButtonXPath) );
        createCardButton.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated( By.xpath("//textarea[@placeholder=\"Ввести заголовок для этой карточки\"]") ));
        WebElement cardNameField = driver.findElement( By.xpath("//textarea[@placeholder=\"Ввести заголовок для этой карточки\"]") );


        String newCardName = "Валик чемпион! " + new Timestamp(System.currentTimeMillis()).getTime();
        cardNameField.sendKeys(newCardName, Keys.ENTER);

        wait.until(ExpectedConditions.visibilityOfElementLocated( By.xpath("//h2[contains(text(), \"Готово\")]/../..//span[contains(text(), \"" + newCardName + "\")]") ));
        WebElement createdCard = driver.findElement( By.xpath("//h2[contains(text(), \"Готово\")]/../..//span[contains(text(), \"" + newCardName + "\")]") );

        Actions actions = new Actions(driver);
        actions.moveToElement(createdCard).build().perform();

        String editButtonXPath = "//h2[contains(text(), \"Готово\")]/../..//span[contains(text(), \"" + newCardName + "\")]/../..//span[contains(@class,\"icon-edit\")]";
        wait.until(ExpectedConditions.visibilityOfElementLocated( By.xpath(editButtonXPath) ));
        WebElement editButton = driver.findElement( By.xpath(editButtonXPath) );
        editButton.click();

        // TODO ...


        driver.close();
    }
}
