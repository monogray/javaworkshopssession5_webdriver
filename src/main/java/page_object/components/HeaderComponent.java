package page_object.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import page_object.DashboardPage;

public class HeaderComponent {
    // Settings data
    private WebDriver driver;

    // Page structure
    private By loginLink = By.linkText("Log In");

    private By userIconLocator = By.xpath("//a[contains(@aria-label, \"Открыть меню участников\")]");
    private By logOutLinkLocator = By.xpath("//a[contains(text(), \"Выйти\")]");

    public HeaderComponent(WebDriver driver) {
        this.driver = driver;
    }

    // Page logic
    public HeaderComponent logOut() {
        WebDriverWait wait = new WebDriverWait(driver, 10);

        wait.until(ExpectedConditions.visibilityOfElementLocated(userIconLocator));
        WebElement userIcon = driver.findElement(userIconLocator);
        userIcon.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(logOutLinkLocator));
        WebElement logOutLink = driver.findElement(logOutLinkLocator);
        logOutLink.click();

        return this;
    }
}
