package page_object;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
    // Settings data
    private WebDriver driver;

    // Page structure
    private By loginFieldLocator = By.id("user");
    private By passwordFieldLocator = By.id("password");

    public LoginPage (WebDriver driver) {
        this.driver = driver;
    }

    // Page logic
    public DashboardPage login(String userLogin, String userPassword) {
        WebDriverWait wait = new WebDriverWait(driver, 10);

        wait.until(ExpectedConditions.visibilityOfElementLocated(loginFieldLocator));
        WebElement loginField = driver.findElement( loginFieldLocator );

        wait.until(ExpectedConditions.visibilityOfElementLocated(passwordFieldLocator));
        WebElement passwordField = driver.findElement( passwordFieldLocator );

        loginField.sendKeys(userLogin);
        passwordField.sendKeys(userPassword);

        passwordField.submit();

        return new DashboardPage(driver);
    }
}
