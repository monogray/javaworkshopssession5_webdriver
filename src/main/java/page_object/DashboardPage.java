package page_object;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import page_object.components.HeaderComponent;

public class DashboardPage {
    // Settings data
    private WebDriver driver;

    // Page structure
    private By userIconLocator = By.xpath("//span[@title=\"Test (test00176474)\"]");

    private By userNameLocator = By.xpath("//span[@class=\"pop-over-header-title\"]");
    private By testBoardLocator = By.xpath("//div[contains(text(), \"Доска без названия\")]");

    private HeaderComponent header;

    public DashboardPage (WebDriver driver) {
        this.driver = driver;

        header = new HeaderComponent(this.driver);
    }

    // Page logic
    public DashboardPage checkIsLogined(String expectedUserName) {
        // TODO Refactor this
        WebDriverWait wait = new WebDriverWait(driver, 10);

        wait.until(ExpectedConditions.visibilityOfElementLocated(userIconLocator));
        WebElement userIcon = driver.findElement(userIconLocator);
        userIcon.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(userNameLocator));
        WebElement userNameElem = driver.findElement(userNameLocator);
        String userName = userNameElem.getText();

        Assert.assertEquals(expectedUserName, userName);

        return this;
    }

    public DashboardPage openBoard(String boardName) {
        // TODO Refactor this
        WebDriverWait wait = new WebDriverWait(driver, 10);

        wait.until(ExpectedConditions.visibilityOfElementLocated(testBoardLocator));
        WebElement testBoard = driver.findElement(testBoardLocator);
        testBoard.click();

        return this;
    }

    public HeaderComponent getHeader () {
        return this.header;
    }
}
