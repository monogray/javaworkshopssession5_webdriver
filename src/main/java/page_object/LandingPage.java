package page_object;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.swing.*;

public class LandingPage {
    // Settings data
    private WebDriver driver;

    // Page structure
    private String url = "https://trello.com/";
    private By loginLink = By.linkText("Log In");

    private By userIconLocator = By.xpath("//a[contains(@aria-label, \"Открыть меню участников\")]");
    private By userNameLocator = By.xpath("//span[@class=\"pop-over-header-title\"]");
    private By logOutLinkLocator = By.xpath("//a[contains(text(), \"Выйти\")]");

    public LandingPage(WebDriver driver) {
        this.driver = driver;
    }

    // Page logic
    public LandingPage logOutIfLogIned() {
        // TODO Refactor this
        WebDriverWait wait = new WebDriverWait(driver, 10);

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(userIconLocator));
            WebElement userIcon = driver.findElement(userIconLocator);
            userIcon.click();

            wait.until(ExpectedConditions.visibilityOfElementLocated(logOutLinkLocator));
            WebElement logOutLink = driver.findElement(logOutLinkLocator);
            logOutLink.click();
        } catch (Exception e) {

        }

        return this;
    }

    public LandingPage open() {
        driver.get(this.url);

        return this;
    }

    public LoginPage goToLoginPage() {
        WebElement loginButton = this.driver.findElement(this.loginLink);
        loginButton.click();

        return new LoginPage(this.driver);
    }
}
