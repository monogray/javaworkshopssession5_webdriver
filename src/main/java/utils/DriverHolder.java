package utils;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;


public class DriverHolder {

    private static WebDriver driver;

    public DriverHolder() {

    }

    public static WebDriver get() {
        if (driver == null) {
            //TODO add run configuration
            System.setProperty("webdriver.chrome.driver", "/Users/user/Dropbox/Development/java-projects/Libraries/lib/selenium/chromedriver");
            driver = new ChromeDriver();
        }

        return driver;
    }

}
